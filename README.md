# DeliveryAsAService

Delivery As A Service API


## Prerequisites
- [Golang compiler and tools](https://go.dev/doc/install) (this was built against go version go1.16.5 darwin/amd64)
- CMake (optional)
- Curl, Insomnia or some way to make calls to the API
- Install dependencies (go get ./....) (ensure youre in the root directory after getting the source)

## Settings / Configuration
The app needs some settings / configuration to work correctly
- `PORT` Port app should run on 
- `TOTAL_ROBOTS` (unsigned int) To simulate reality, we provide a means to limit the number of available robots to the service (eg 5)
- `MAX_DELIVERY_DISTANCE` (unsigned int) We simulate deliveries by a factor of the from and destination deliver locations, to prevent absurd wait times, this provides a way to limit how long we'd have to wait. (currently MAX_DELIVERY_DISTANCE * Seconds)
- `FIXED_PRICE` (unsigned int) We currently return a fixed price, this is in cents [in Fowler money style](https://verraes.net/2011/04/fowler-money-pattern-in-php/])
- `CURRENCY` (string, default USD) the currency money in the system is represented in.
## How to run
- Setup configuration via environmental variables (see contents of `.env.sample`), you can prefix later commands with env variables or export to current shell)
-- Prefixing environment variables before commands will look like `PORT=8000 TOTAL_ROBOTS=5 MAX_DELIVERY_DISTANCE=100 go run main.go`
- Extract source from archive or clone from [repository](https://gitlab.com/oneeyedsunday/deliveryasaservice)
- Boot up server (go run main.go) or (make run)
- Alternatively run the executable (`./main`)

## How to test
- Run automated tests via `go test ./...` or `make test`


## How app works
- The app is exposed as a http api under `http://localhost:{PORT}/api/v1`
- Request a delivery via `POST http://localhost:{PORT}/api/v1`
- Accept delivery via `POST http://localhost:{PORT}/api/v1/delivery/{deliveryId}/accept`


### Sample requests via CURL

The following samples assume the app is running in port 8000 under localhost

Make a delivery request 

```sh
curl -X POST http://localhost:8000/api/v1/delivery -H "Content-Type: application/json" -d '{ "from": { "x": 4, "y": 5 }, "destination": {"x": 23, "y": 23}  }'
```


Accept a delivery request with id of 1

```sh
curl -X POST http://localhost:8000/api/v1/delivery/10/accept -H "Content-Type: application/json"       
```
