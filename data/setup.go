package data

import (
	"os"
	"strconv"

	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
	"gitlab.com/oneeyedsunday/deliveryasaservice/repository"
)

func SetupApp() error {
	numRobots, err := strconv.ParseUint(os.Getenv("TOTAL_ROBOTS"), 10, 32)

	if err != nil {
		return err
	}

	return stageRobots(uint(numRobots))
}

func stageRobots(numberOfRobots uint) error {
	repository.EnsureInit(numberOfRobots)
	for i := 0; i < int(numberOfRobots); i++ {
		r := models.Robot{Status: models.AVAILABLE, ID: models.AppID(i + 1)}
		if err := repository.StoreRobot(r); err != nil {
			return err
		}
	}

	return nil
}
