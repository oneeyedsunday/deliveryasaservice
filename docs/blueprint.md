### Generic Delivery As A Service API


#### Inputs
- Pickup location
- Dropoff location

#### Output
- Fee (flat)

#### Endpoints
- Request Quote for coords
- Accept quote (by id)

### Models
- Location ( A location is a cartesian coordinate)
- Delivery ( A delivery is an attempt to send stuff between two locations)
- Robot

### Flow
- We set up a number of available robots on app start
- We set up flat fee
- When a quote is requested (via an endpoint `POST /api/delivery/request` ), we:
 - Check if robots available
 - If available, we assign the delivery an ID, we return flat fee
 - Else, we return an error and ask user to check back
-  Customer can accept quote, if quote is accepted (via endpoint `POST /api/delivery/{id}/accept`)
 - We assign to available robot,
 - If robot is now unavailable, we show user the error


### Considerations
- What happens if someone else orders between an initial order and accepting said order, resulting in unavilable robot
- Well, we can handle this is a variety of ways:
 - We could either mark robots as taken once a quote comes in, but could leave the business short.
 - Or just assume customers will accept quotes in negligible periods of time.
- We also want to ratelimit the request endpoint by IP