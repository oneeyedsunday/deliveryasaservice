package dto

import (
	"time"

	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

type LocationRequest struct {
	X float64 `json:"x" validate:"required"`
	Y float64 `json:"y" validate:"required"`
}

type DeliveryRequest struct {
	From        LocationRequest `json:"from" validate:"required"`
	Destination LocationRequest `json:"destination" validate:"required"`
}

type DeliveryResponse struct {
	CreatedAt    time.Time       `json:"createdAt"`
	CompletedAt  time.Time       `json:"completedAt"`
	TransitAt    time.Time       `json:"transitAt"`
	ID           models.AppID    `json:"id"`
	LocationMeta DeliveryRequest `json:"locationMeta"`
	Quote        models.Quote    `json:"quote"`
}
