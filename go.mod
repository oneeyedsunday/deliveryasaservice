module gitlab.com/oneeyedsunday/deliveryasaservice

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/gwuah/postmates v0.0.0-20210110205606-3c978672f2d0
	github.com/joho/godotenv v1.4.0
	github.com/stretchr/testify v1.7.0
)
