package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/oneeyedsunday/deliveryasaservice/dto"
	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

func (h *Handler) requestDelivery(c *gin.Context) {
	var data dto.DeliveryRequest

	if err := c.ShouldBind(&data); err != nil {
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"message": "failure",
			"err":     err,
		})
		return
	}

	response, err := h.Services.RequestDelivery(data)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failure to request delivery",
			"err":     err.Error(),
		})
		return
	}

	c.JSON(http.StatusAccepted, gin.H{
		"message": "success",
		"data":    response,
	})
}

func (h *Handler) acceptDelivery(c *gin.Context) {
	id, err := models.AppIDFromString(c.Param("id"))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid delivery id",
		})
		return
	}

	delivery, err := h.Services.AcceptDelivery(id)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failure to accept delivery",
			"err":     err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data":    delivery,
	})
}
