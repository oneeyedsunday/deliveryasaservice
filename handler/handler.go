package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/oneeyedsunday/deliveryasaservice/repository"
	"gitlab.com/oneeyedsunday/deliveryasaservice/services"
)

type Handler struct {
	Services *services.Services
}

func New() *Handler {
	services := services.New(&repository.RobotRepo{}, &repository.DeliveryRepo{})
	return &Handler{
		Services: services,
	}
}

func (h *Handler) Register(v1 *gin.RouterGroup) {
	// register routes here

	v1.POST("/delivery", h.requestDelivery)
	v1.POST("/delivery/:id/accept", h.acceptDelivery)
}
