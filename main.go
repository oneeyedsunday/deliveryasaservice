package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/oneeyedsunday/deliveryasaservice/data"
	"gitlab.com/oneeyedsunday/deliveryasaservice/handler"
	"gitlab.com/oneeyedsunday/deliveryasaservice/server"
)

func main() {
	ENV := os.Getenv("ENV")
	if ENV == "" {
		err := godotenv.Load()

		if err != nil {
			log.Fatal("Error loading .env file", err)
		}
	}

	err := data.SetupApp()

	if err != nil {
		log.Fatal("ailed to setup App", err)
	}

	s := server.New()
	h := handler.New()

	routes := s.Group("/api/v1")
	h.Register(routes)
	s.NoRoute(h.NoRoute)

	server.Start(&s, &server.Config{
		Port: fmt.Sprintf(":%s", os.Getenv("PORT")),
	})
}
