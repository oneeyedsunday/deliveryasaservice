package models

import "time"

const (
	NOTSTARTED = iota
	INPROGRESS
	DELIVERED
)

const (
	ErrCouldNotCreateDelivery        AppError = "failed to create delivery"
	ErrCouldNotCompleteDelivery      AppError = "failed to complete delivery"
	ErrDeliveryNotFound              AppError = "failed to find delivery"
	ErrCannotUpdateCompletedDelivery AppError = "cannot update completed delivery"
)

type DeliveryStatus uint

type Delivery struct {
	From        Location
	To          Location
	ID          AppID
	RobotID     AppID
	Status      DeliveryStatus
	CreatedAt   time.Time
	CompletedAt time.Time
	TransitAt   time.Time
	Quote       Quote
}

type DeliveryStore interface {
	CreateDelivery(robotID AppID, from, to Location, quote Quote) (*Delivery, error)
	CompleteDelivery(delivery Delivery) (*Delivery, error)
	FindDeliveryById(deliveryID AppID) (*Delivery, error)
	UpdateDelivery(delivery Delivery) error
}
