package models

// constant error impl.
// see https://travix.io/errors-derived-from-constants-in-go-fda6748b4072

type AppError string

func (e AppError) Error() string {
	return string(e)
}
