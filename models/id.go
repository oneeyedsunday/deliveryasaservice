package models

import "strconv"

type AppID uint16

func AppIDFromString(raw string) (AppID, error) {
	val, err := strconv.ParseUint(raw, 10, 16)

	if err != nil {
		// TODO wrap such errors in domain errors
		return 0, err
	}

	return AppID(val), nil
}
