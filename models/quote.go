package models

type Quote struct {
	Currency Currency
	Cents    uint64
}

func NewQuote(curr string, cents uint64) *Quote {
	return &Quote{
		Currency: Currency(curr),
		Cents:    cents,
	}
}
