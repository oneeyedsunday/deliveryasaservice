package models

const (
	AVAILABLE = iota
	DELIVERING
	RETURNING
)

const (
	ErrNoAvailableRobotFound AppError = "no available robot"
	ErrRobotNotFound         AppError = "robot not found"
	ErrRobotUnavailable      AppError = "robot unavailable"
)

type RobotStatus uint

type Robot struct {
	Status RobotStatus
	ID     AppID
}

type RobotStore interface {
	FindNextAvailableRobot() (*Robot, error)
	FindRobotById(AppID) (*Robot, error)
	UpdateRobot(Robot) error
}
