package repository

import (
	"time"

	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

type DeliveryRepo struct{}

var DeliveriesById map[models.AppID]*models.Delivery = make(map[models.AppID]*models.Delivery, 1000)

func (repo *DeliveryRepo) CreateDelivery(robotID models.AppID, from, to models.Location, quote models.Quote) (*models.Delivery, error) {
	delivery := &models.Delivery{
		RobotID:   robotID,
		From:      from,
		To:        to,
		ID:        models.AppID(len(DeliveriesById) + 1),
		Status:    models.NOTSTARTED,
		CreatedAt: time.Now().UTC(),
		Quote:     quote,
	}

	DeliveriesById[delivery.ID] = delivery
	return delivery, nil
}

func (repo *DeliveryRepo) CompleteDelivery(delivery models.Delivery) (*models.Delivery, error) {
	existingDelivery, ok := DeliveriesById[delivery.ID]
	if !ok {
		return nil, models.ErrCouldNotCompleteDelivery
	}

	existingDelivery.Status = models.DELIVERED
	existingDelivery.CompletedAt = time.Now().UTC()

	DeliveriesById[delivery.ID] = existingDelivery

	return existingDelivery, nil
}

func (repo *DeliveryRepo) FindDeliveryById(id models.AppID) (*models.Delivery, error) {
	existingDelivery, ok := DeliveriesById[id]
	if !ok {
		return nil, models.ErrDeliveryNotFound
	}

	return existingDelivery, nil
}

func (repo *DeliveryRepo) UpdateDelivery(delivery models.Delivery) error {
	d, err := repo.FindDeliveryById(delivery.ID)

	if err != nil {
		return err
	}

	if d.Status == models.DELIVERED {
		return models.ErrCannotUpdateCompletedDelivery
	}

	d.Status = delivery.Status
	d.From = delivery.From
	d.To = delivery.To
	d.RobotID = delivery.RobotID

	DeliveriesById[delivery.ID] = d

	return nil
}

var _ models.DeliveryStore = (*DeliveryRepo)(nil)
