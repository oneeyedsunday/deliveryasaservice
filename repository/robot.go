package repository

import (
	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

type RobotRepo struct{}

var RobotsById map[models.AppID]*models.Robot

func EnsureInit(numRobots uint) {
	if RobotsById != nil {
		return
	}
	RobotsById = make(map[models.AppID]*models.Robot, numRobots)
}

func StoreRobot(robot models.Robot) error {
	RobotsById[robot.ID] = &robot

	return nil
}

func (repo *RobotRepo) FindNextAvailableRobot() (*models.Robot, error) {
	for _, j := range RobotsById {
		if j.Status == models.AVAILABLE {
			return j, nil
		}
	}
	return nil, models.ErrNoAvailableRobotFound
}

func (repo *RobotRepo) FindRobotById(id models.AppID) (*models.Robot, error) {
	existingRobot, ok := RobotsById[id]
	if !ok {
		return nil, models.ErrRobotNotFound
	}

	return existingRobot, nil
}

func (repo *RobotRepo) UpdateRobot(robot models.Robot) error {
	r, err := repo.FindRobotById(robot.ID)

	if err != nil {
		return err
	}

	r.Status = robot.Status

	RobotsById[r.ID] = r

	return nil
}

var _ models.RobotStore = (*RobotRepo)(nil)
