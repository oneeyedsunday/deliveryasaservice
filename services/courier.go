package services

import (
	"log"
	"math"
	"os"
	"strconv"
	"time"

	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

func (s *Services) SimulateDelivery(d models.Delivery) {
	simDuration, err := simulateDeliveryTime(d)
	log.Printf("simulator will run for %v \n", simDuration)
	ticker := time.NewTicker(simDuration)
	if err != nil {
		log.Println("failed to get simulation duration, stopping sim")
		return
	}
	log.Println("simulating delivery time")

	<-ticker.C
	ticker.Stop()

	_, err = s.deliveryRepo.CompleteDelivery(d)

	if err != nil {
		log.Printf("failed to complete delivery %v\n", d.ID)
		return
	}

	log.Printf("delivery %v completed\n", d.ID)
}

func simulateDeliveryTime(d models.Delivery) (time.Duration, error) {
	maxDeliveryDelta, err := strconv.ParseUint(os.Getenv("MAX_DELIVERY_DISTANCE"), 10, 64)

	if err != nil {
		return time.Second, err
	}
	xDelta := math.Abs(d.From.X - d.To.X)
	yDelta := math.Abs(d.From.Y - d.To.Y)
	distance := math.Max(xDelta*yDelta, float64(maxDeliveryDelta))

	return time.Second * time.Duration(distance), nil
}
