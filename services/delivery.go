package services

import (
	"log"
	"time"

	"gitlab.com/oneeyedsunday/deliveryasaservice/dto"
	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

func (s *Services) RequestDelivery(dlvReq dto.DeliveryRequest) (*dto.DeliveryResponse, error) {
	r, err := s.robotRepo.FindNextAvailableRobot()

	if err != nil {
		log.Printf("failed to find available robot")
		return nil, err
	}

	log.Default().Printf("found available robot with id: %v", r.ID)

	q, err := s.GetQuote(dlvReq)

	if err != nil {
		log.Printf("failed to get quote for delivery")
		return nil, err
	}

	d, err := s.deliveryRepo.CreateDelivery(r.ID, models.Location(dlvReq.From), models.Location(dlvReq.Destination), *q)

	if err != nil {
		log.Printf("failed to create delivery")
		return nil, err
	}

	log.Default().Printf("obtained quote for delivery %v: %v", d.ID, q)
	return &dto.DeliveryResponse{
		CreatedAt:    d.CreatedAt,
		ID:           d.ID,
		LocationMeta: dlvReq,
		Quote:        *q,
	}, nil
}

func (s *Services) AcceptDelivery(id models.AppID) (*dto.DeliveryResponse, error) {
	d, err := s.deliveryRepo.FindDeliveryById(id)

	if err != nil {
		log.Printf("failed to find delivery")
		return nil, err
	}

	if d.Status == models.INPROGRESS || d.Status == models.DELIVERED {
		return newDeliveryResponse(d), nil
	}

	r, err := s.robotRepo.FindRobotById(d.RobotID)

	if err != nil {
		log.Printf("failed to find robot specified in delivery")
		return nil, err
	}

	if r.Status == models.AVAILABLE || r.Status == models.RETURNING {
		// assume we can provision a returning robot
		// and it'd be cheaper than changing the delivery to use another robot

		err = s.useRobotForDelivery(r, d)

		if err != nil {
			return nil, err
		}

		return newDeliveryResponse(d), nil
	}

	availR, err := s.robotRepo.FindNextAvailableRobot()

	if err != nil {
		log.Printf("failed to find available robot when accepting delivery")
		return nil, err
	}

	err = s.useRobotForDelivery(availR, d)

	if err != nil {
		return nil, err
	}

	return newDeliveryResponse(d), nil
}

func (s *Services) useRobotForDelivery(r *models.Robot, d *models.Delivery) error {
	d.Status = models.INPROGRESS
	d.TransitAt = time.Now().UTC()
	r.Status = models.DELIVERING

	err := s.deliveryRepo.UpdateDelivery(*d)

	if err != nil {
		log.Printf("failed to update delivery")
		return err
	}
	err = s.robotRepo.UpdateRobot(*r)

	if err != nil {
		log.Printf("failed to update robot")
		return err
	}

	// fire a go func that completes the delivery after a set of seconds
	go s.SimulateDelivery(*d)

	return nil
}

func newDeliveryResponse(d *models.Delivery) *dto.DeliveryResponse {
	return &dto.DeliveryResponse{
		CreatedAt: d.CreatedAt,
		TransitAt: d.TransitAt,
		ID:        d.ID,
		LocationMeta: dto.DeliveryRequest{
			From:        dto.LocationRequest{X: d.From.X, Y: d.From.Y},
			Destination: dto.LocationRequest{X: d.To.X, Y: d.To.Y},
		},
		Quote: d.Quote,
	}
}
