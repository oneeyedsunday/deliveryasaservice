package services

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/oneeyedsunday/deliveryasaservice/dto"
	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

type FakeDeliveryRepo struct {
	WantErr bool
}

type FakeRobotRepo struct {
	WantErr    bool
	WantID     models.AppID
	WantStatus models.RobotStatus
}

func (f *FakeRobotRepo) FindNextAvailableRobot() (*models.Robot, error) {
	if f.WantErr {
		return nil, errors.New("some error")
	}

	return &models.Robot{
		ID:     f.WantID,
		Status: f.WantStatus,
	}, nil
}

func (f *FakeRobotRepo) FindRobotById(id models.AppID) (*models.Robot, error) {
	if f.WantErr {
		return nil, errors.New("some error")
	}

	return &models.Robot{
		ID:     f.WantID,
		Status: f.WantStatus,
	}, nil
}

func (f *FakeRobotRepo) UpdateRobot(r models.Robot) error {
	if f.WantErr {
		return errors.New("some error")
	}

	return nil
}

func (f *FakeDeliveryRepo) CreateDelivery(robotID models.AppID, from, to models.Location, quote models.Quote) (*models.Delivery, error) {
	if f.WantErr {
		return nil, errors.New("some error")
	}

	d := models.Delivery{}

	return &d, nil
}

func (f *FakeDeliveryRepo) CompleteDelivery(delivery models.Delivery) (*models.Delivery, error) {
	if f.WantErr {
		return nil, errors.New("some error")
	}

	delivery.Status = models.DELIVERED
	delivery.CompletedAt = time.Now().UTC()

	return &delivery, nil
}

func (f *FakeDeliveryRepo) FindDeliveryById(id models.AppID) (*models.Delivery, error) {
	if f.WantErr {
		return nil, errors.New("some error")
	}

	d := models.Delivery{}

	return &d, nil
}

func (f *FakeDeliveryRepo) UpdateDelivery(delivery models.Delivery) error {
	if f.WantErr {
		return errors.New("some error")
	}

	return nil
}

func Test_DeliveryService_RequestDelivery(t *testing.T) {
	var r = &FakeRobotRepo{}
	var d = &FakeDeliveryRepo{}
	var s = &Services{
		robotRepo:    r,
		deliveryRepo: d,
	}

	tests := []struct {
		name       string
		wantErr    bool
		envSetFunc func(t *testing.T)
		dlvReq     dto.DeliveryRequest
	}{
		{
			name:    "failure",
			wantErr: true,
			dlvReq:  dto.DeliveryRequest{},
			envSetFunc: func(lt *testing.T) {
				lt.Setenv("FIXED_PRICE", "")
			},
		},
		{
			name:    "success",
			wantErr: false,
			dlvReq:  dto.DeliveryRequest{},
			envSetFunc: func(lt *testing.T) {
				lt.Setenv("FIXED_PRICE", "200")
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.envSetFunc(t)
			got, err := s.RequestDelivery(tt.dlvReq)
			if !tt.wantErr {
				require.Equal(t, got.LocationMeta, tt.dlvReq)
			} else {
				require.NotNil(t, err)
			}

		})
	}
}

func Test_DeliveryService_AcceptDelivery(t *testing.T) {
	tests := []struct {
		name            string
		id              models.AppID
		wantErr         bool
		wantRobotErr    bool
		wantDeliveryErr bool
	}{
		{
			name:            "failure-delivery-not-found",
			id:              models.AppID(3),
			wantErr:         true,
			wantDeliveryErr: true,
			wantRobotErr:    false,
		},
		{
			name:            "failure-no-more-robots",
			id:              models.AppID(2),
			wantErr:         true,
			wantDeliveryErr: false,
			wantRobotErr:    true,
		},
		{
			name:            "success",
			id:              models.AppID(1),
			wantErr:         false,
			wantDeliveryErr: false,
			wantRobotErr:    false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Services{
				robotRepo:    &FakeRobotRepo{WantErr: tt.wantRobotErr},
				deliveryRepo: &FakeDeliveryRepo{WantErr: tt.wantDeliveryErr},
			}
			got, err := s.AcceptDelivery(tt.id)
			if !tt.wantErr {
				require.WithinDuration(t, got.TransitAt, time.Now().UTC(), time.Minute)
			} else {
				require.NotNil(t, err)
			}
		})
	}
}
