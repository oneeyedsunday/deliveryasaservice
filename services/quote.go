package services

import (
	"os"
	"strconv"

	"gitlab.com/oneeyedsunday/deliveryasaservice/dto"
	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

func (s *Services) GetQuote(dlvReq dto.DeliveryRequest) (*models.Quote, error) {
	// This impl just returns the fixed price regardless

	cents, err := getFixedPrice()
	if err != nil {
		return nil, err
	}

	return models.NewQuote(getCurrency(), cents), nil
}

func getFixedPrice() (uint64, error) {
	return strconv.ParseUint(os.Getenv("FIXED_PRICE"), 10, 64)
}

func getCurrency() string {
	curr := os.Getenv("CURRENCY")
	if len(curr) == 0 {
		return "USD"
	}

	return curr
}
