package services

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/oneeyedsunday/deliveryasaservice/dto"
	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
)

func makeDeliveryRequestFromLocations(from, to dto.LocationRequest) dto.DeliveryRequest {
	return dto.DeliveryRequest{
		From:        from,
		Destination: to,
	}
}

func makeLocationRequest(x, y float64) dto.LocationRequest {
	return dto.LocationRequest{
		X: x,
		Y: y,
	}
}

func Test_Quote(t *testing.T) {
	t.Setenv("FIXED_PRICE", "200")
	loc1_1 := makeLocationRequest(1, 1)
	loc3_10 := makeLocationRequest(3, 10)
	loc12_minus_12 := makeLocationRequest(12, -12)
	tests := []struct {
		name    string
		want    *models.Quote
		dlvReq  dto.DeliveryRequest
		wantErr bool
	}{
		{
			name:    "return expected price for same location",
			wantErr: false,
			want:    &models.Quote{Currency: "USD", Cents: 200},
			dlvReq:  makeDeliveryRequestFromLocations(loc1_1, loc1_1),
		},
		{
			name:    "return expected price for nearby locations",
			wantErr: false,
			want:    &models.Quote{Currency: "USD", Cents: 200},
			dlvReq:  makeDeliveryRequestFromLocations(loc1_1, loc3_10),
		},
		{
			name:    "return expected price for far away locations",
			wantErr: false,
			want:    &models.Quote{Currency: "USD", Cents: 200},
			dlvReq:  makeDeliveryRequestFromLocations(loc1_1, loc12_minus_12),
		},
	}
	s := &Services{
		robotRepo:    nil,
		deliveryRepo: nil,
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.GetQuote(tt.dlvReq)
			if !tt.wantErr {
				require.Equal(t, tt.want, got)
			} else {
				require.NotNil(t, err)
			}
		})
	}
}
