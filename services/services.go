package services

import (
	"gitlab.com/oneeyedsunday/deliveryasaservice/dto"
	"gitlab.com/oneeyedsunday/deliveryasaservice/models"
	"gitlab.com/oneeyedsunday/deliveryasaservice/repository"
)

type AppServices interface {
	RequestDelivery(dto.DeliveryRequest) (*dto.DeliveryResponse, error)
	GetQuote(dto.DeliveryRequest) (*models.Quote, error)
	AcceptDelivery(models.AppID) (*dto.DeliveryResponse, error)
}

type Services struct {
	robotRepo    models.RobotStore
	deliveryRepo models.DeliveryStore
}

func New(robotRepo *repository.RobotRepo, deliveryRepo *repository.DeliveryRepo) *Services {
	return &Services{
		robotRepo:    robotRepo,
		deliveryRepo: deliveryRepo,
	}
}

var _ AppServices = (*Services)(nil)
